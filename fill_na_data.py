import pandas as pd
from hydra.experimental import compose, initialize

# read data
data = pd.read_parquet(snakemake.input[0])

# get config
initialize(config_path="../../conf")
cfg = compose("config.yaml")['preprocessing']

# fill na values with mode value
cols_with_nans = [col_ for col_, val_ in (data.isna().sum() > 0).items() if val_]
for col_ in cols_with_nans:
    match data[col_].dtype:
        case 'object':
            fill_val = data[col_].agg(cfg['object_fields'])
        case 'float':
            fill_val = data[col_].agg(cfg['float_fields'])
        case 'int':
            fill_val = data[col_].agg(cfg['int_fields'])

    if type(fill_val) == pd.Series:
        fill_val = fill_val.values[0]
    data[col_] = data[col_].fillna(fill_val)

# save data
data.to_parquet(snakemake.output[0])
