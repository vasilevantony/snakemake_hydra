import pandas as pd
from catboost import CatBoostClassifier
from hydra.experimental import compose, initialize

# load data
data = pd.read_parquet(snakemake.input[0])

# get config
initialize(config_path="../../conf")
cfg = compose("config.yaml")['model']

y = data['health']
X = data.drop(['health', 'tree_id'], axis=1)

cat_cols = [col_ for col_, dtype_ in  X.dtypes.items() if dtype_ == 'object']

# fit a model
model = CatBoostClassifier(cat_features=cat_cols, **cfg)
model.fit(X, y)

# save a model
model.save_model(snakemake.output[0])
