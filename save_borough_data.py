import pandas as pd

# read data
data = pd.read_csv(snakemake.input[0])

# split data by borough
for borough_, df_ in data.groupby('borough'):
    df_.to_parquet(f'data/{borough_}.parquet')
