# snakemake_hydra
Репозиторий по Snakemake и Hydra.  
  
## Необходимые данные
Необходимо создать в корне проекта папку 'data' и поместить в нее файл https://www.kaggle.com/datasets/new-york-city/ny-2015-street-tree-census-tree-data/data?select=2015-street-tree-census-tree-data.csv

## Шаги пайплайна:
1. Данные разбиваются на отдельные датафреймы по значениям в borough и сохраняются в файлы parquet
2. По каждому borough заполняются пропущенные значения модой соответствующего столбца
3. Получившиеся файлы объеденяются обратно в единый датафрейм
4. Обучается модель CatBoostClassifier на таргете health
  

## Визуализация графа:

<img src="dag.svg">