import os

BOROUGHS = ["Queens", "Brooklyn", "Staten Island", "Bronx", "Manhattan"]

rule all:
    input:
        "cb_model.cbm"

rule split:
    input:
        "data/2015-street-tree-census-tree-data.csv"
    output:
        expand("data/{borough}.parquet", borough=BOROUGHS)
    script:
        "save_borough_data.py"

rule fill_na:
    input:
        "data/{borough}.parquet"
    output:
        "data/{borough}_filled.parquet"
    script:
        "fill_na_data.py"

rule union:
    input:
        expand("data/{borough}_filled.parquet", borough=BOROUGHS)
    output:
        "data/train_data.parquet"
    script:
        "union.py"

rule train:
    input:
        "data/train_data.parquet"
    output:
        "cb_model.cbm"
    threads: 3
    resources: gpu=1
    script:
        "model.py"
