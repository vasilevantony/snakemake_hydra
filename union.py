import pandas as pd

# read data from files
res_list = []
for file_ in snakemake.input:
     res_list.append(pd.read_parquet(file_))

# concat dataframes
res_df = pd.concat(res_list).reset_index(drop=True)

# save data
res_df.to_parquet(snakemake.output[0])
